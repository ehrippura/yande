//
//  DownloadQueueCell.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/14.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

class DownloadQueueCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
}
