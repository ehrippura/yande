//
//  ImageScrollView.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/30.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

final class ImageScrollView: UIScrollView {

    private let imageView = UIImageView()
    private var imageViewBottomConstraint = NSLayoutConstraint()
    private var imageViewLeadingConstraint = NSLayoutConstraint()
    private var imageViewTopConstraint = NSLayoutConstraint()
    private var imageViewTrailingConstraint = NSLayoutConstraint()

    @IBInspectable var maxScaleRate: CGFloat = 2.0 {
        didSet {
            updateZoomScaleSettings()
        }
    }

    var image: UIImage? {
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            updateZoomScaleSettings()
        }
        get {
            imageView.image
        }
    }

    required init(image: UIImage) {
        super.init(frame: .zero)
        self.image = image
        initView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }

    private func initView() {

        addSubview(imageView)

        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageViewLeadingConstraint = imageView.leadingAnchor.constraint(equalTo: leadingAnchor)
        imageViewTrailingConstraint = imageView.trailingAnchor.constraint(equalTo: trailingAnchor)
        imageViewTopConstraint = imageView.topAnchor.constraint(equalTo: topAnchor)
        imageViewBottomConstraint = imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([imageViewLeadingConstraint, imageViewTrailingConstraint, imageViewTopConstraint, imageViewBottomConstraint])

        contentInsetAdjustmentBehavior = .never // Adjust content according to safe area if necessary
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        alwaysBounceHorizontal = true
        alwaysBounceVertical = true
        delegate = self

        let gesture = UITapGestureRecognizer(target: self, action: #selector(doubleTap))
        gesture.numberOfTapsRequired = 2
        addGestureRecognizer(gesture)
    }

    @objc func doubleTap() {
        if zoomScale != minimumZoomScale {
            setZoomScale(minimumZoomScale, animated: true)
        } else {
            setZoomScale(maxScaleRate, animated: true)
        }
    }

    // MARK: - Helper methods
    func updateZoomScaleSettings() {
        let widthScale = frame.size.width / imageView.bounds.width
        let heightScale = frame.size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        minimumZoomScale = min(minScale, 1.0)
        maximumZoomScale = max(minimumZoomScale * maxScaleRate, 1.0)
        zoomScale = minScale
    }
}

extension ImageScrollView: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        imageView
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let yOffset = max(0, (bounds.size.height - imageView.frame.height) / 2)
        imageViewTopConstraint.constant = yOffset
        imageViewBottomConstraint.constant = yOffset

        let xOffset = max(0, (bounds.size.width - imageView.frame.width) / 2)
        imageViewLeadingConstraint.constant = xOffset
        imageViewTrailingConstraint.constant = xOffset

        layoutIfNeeded()
    }
}

