//
//  LabelItemCell.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/7.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

class LabelItemCell: UICollectionViewCell {

    static let reuseIdentifier = "list-cell-reuse-identifier"

    @IBOutlet var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.layer.cornerRadius = 5.0
        prepareUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 5.0
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = nil
        label.textColor = UIColor.label
        label.text = nil
    }

    override var isHighlighted: Bool {
        willSet {
            if newValue {
                contentView.backgroundColor = UIColor.systemGray5
            } else {
                contentView.backgroundColor = nil
            }
        }
    }

    private func prepareUI() {
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.font = .preferredFont(forTextStyle: .body)
        label.textColor = UIColor.label
        label.textAlignment = .center
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
}
