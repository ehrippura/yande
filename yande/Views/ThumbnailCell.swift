//
//  ThumbnailCell.swift
//  yande
//
//  Created by Wayne Lin on 2014/7/20.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import UIKit
import Combine

class ThumbnailCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var textLabel: UILabel!

    private var subscriptions: AnyCancellable?

    weak var imageDownloader: ImageDownloader?

    @Published var imageURL: URL = URL(string: "https://")!

    override func awakeFromNib() {
        super.awakeFromNib()
        prepareCombine()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        textLabel.text = nil
        textLabel.textColor = UIColor.label
    }

    override var isHighlighted: Bool {
        willSet {
            if newValue {
                contentView.backgroundColor = UIColor(named: "selected_background")
            } else {
                contentView.backgroundColor = nil
            }
        }
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                
                UIView.animate(
                    withDuration: 0.28,
                    delay: 0.0,
                    usingSpringWithDamping: 0.5,
                    initialSpringVelocity: 0.7,
                    animations: {

                    self.imageView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                    self.textLabel.textColor = .white

                }, completion: nil)

                contentView.backgroundColor = UIColor(named: "selected_background")

            } else {
                UIView.animate(withDuration: 0.28) {
                    self.imageView.transform = .identity
                    self.textLabel.textColor = UIColor.label
                    self.contentView.backgroundColor = nil
                }
            }
        }
    }

    private func prepareCombine() {

        subscriptions = $imageURL
            .dropFirst()
            .map { [weak self] url -> AnyPublisher<UIImage, ImageDownloader.ImageDownloaderError> in
                guard let imageLoader = self?.imageDownloader else {
                    return Empty(completeImmediately: false)
                        .eraseToAnyPublisher()
                }
                return imageLoader.image(url: url)
            }
            .switchToLatest()
            .map { val -> Optional<UIImage> in
                return Optional.some(val)
            }
            .replaceError(with: UIImage())
            .assign(to: \.image, on: imageView)
    }
}
