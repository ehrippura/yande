//
//  PictureFlowLayout.swift
//  yande
//
//  Created by Wayne Lin on 2017/8/5.
//  Copyright © 2017 Eternal Wind. All rights reserved.
//

import UIKit

protocol PictureFlowLayoutDelegate: AnyObject {
    func collectionView(_ collectionView: UICollectionView, heightForPictureAt indexPath: IndexPath, width: CGFloat) -> CGFloat
}

class PictureFlowLayout: UICollectionViewLayout {

    weak var delegate: PictureFlowLayoutDelegate!

    var numberOfColumns = 6 {
        didSet {
            invalidateLayout()
        }
    }

    var cellSpacing: CGFloat = 10.0 {
        didSet {
            invalidateLayout()
        }
    }

    private var attributesCollection: [UICollectionViewLayoutAttributes] = []

    private var contentHeight: CGFloat = 0.0

    private var contentWidth: CGFloat {
        let insets = collectionView!.contentInset
        return collectionView!.bounds.width - (insets.left + insets.right)
    }

    private var xOffset: [CGFloat] = []

    private var yOffset: [CGFloat] = []

    override var collectionViewContentSize: CGSize {
        CGSize(width: contentWidth, height: contentHeight)
    }

    override func prepare() {
        guard attributesCollection.isEmpty else {
            return
        }

        let inset = collectionView!.safeAreaInsets
        
        let columnWidth = (collectionView!.bounds.width - inset.left - inset.right - (CGFloat(numberOfColumns + 1) * cellSpacing)) / CGFloat(numberOfColumns)

        xOffset = (0 ..< numberOfColumns).map { column -> CGFloat in
            (cellSpacing * CGFloat(column + 1)) + (CGFloat(column) * columnWidth) + inset.left
        }
        yOffset = [CGFloat](repeating: cellSpacing, count: numberOfColumns)

        for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            addObjects(at: indexPath)
        }
    }

    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        updateItems
            .filter { $0.updateAction == .insert }
            .compactMap { $0.indexPathAfterUpdate }
            .filter { $0.item >= attributesCollection.count }
            .sorted { $0.item < $1.item }
            .forEach(addObjects)
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if attributesCollection.isEmpty {
            return nil
        }

        return attributesCollection.filter {
            rect.intersects($0.frame)
        }
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        attributesCollection[indexPath.item]
    }

    override func invalidateLayout() {
        attributesCollection.removeAll()
        contentHeight = 0.0
        super.invalidateLayout()
    }

    private func addObjects(at indexPath: IndexPath) {
        
        let inset = collectionView!.safeAreaInsets
        let columnWidth = (collectionView!.bounds.width - inset.left - inset.right - (CGFloat(numberOfColumns + 1) * cellSpacing)) / CGFloat(numberOfColumns)

        // choose index
        let indexColumn = yOffset.firstIndex(of: yOffset.min()!) ?? 0

        let photoHeight = delegate.collectionView(collectionView!, heightForPictureAt: indexPath, width: columnWidth)
        let frame = CGRect(x: xOffset[indexColumn], y: yOffset[indexColumn], width: columnWidth, height: photoHeight)

        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = frame
        attributesCollection.append(attributes)

        contentHeight = max(contentHeight, frame.maxY)
        yOffset[indexColumn] += photoHeight + cellSpacing
    }
}

