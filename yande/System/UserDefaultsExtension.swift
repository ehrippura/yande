//
//  UserDefaultsExtension.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/30.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation

extension UserDefaults {
    /// 使用 plist 檔案初始化使用者設定
    @discardableResult
    func configureDefaultPreference(resourceName name: String) -> Bool {
        guard let filename = Bundle.main.path(forResource: name, ofType: "plist"),
            let pref = NSDictionary(contentsOfFile: filename) as? [String : Any] else {
                return false
        }
        let userDefault = UserDefaults.standard
        userDefault.register(defaults: pref)
        return true
    }
}

