//
//  StringInterpolationExtension.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/5.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation
import CoreGraphics
import Danbooru

extension String.StringInterpolation {

    mutating func appendInterpolation(size: CGSize) {
        appendLiteral("\(Int(size.width)) x \(Int(size.height))")
    }

    mutating func appendInterpolation(filename post: Post) {
        let host = post.image.url.host ?? ""
        let id = post.id
        let ext: String
        if let fileExt = post.fileExtension {
            ext = fileExt
        } else {
            ext = (post.image.url.lastPathComponent as NSString).pathExtension
        }
        appendLiteral("\(host)_\(id).\(ext)")
    }
}
