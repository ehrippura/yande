//
//  SceneDelegate.swift
//  yande
//
//  Created by Wayne Lin on 2019/10/12.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var mainCoordinator: ImageBrowseCoordinator!

    #if targetEnvironment(macCatalyst)
    private lazy var toolbar: NSToolbar = {
        let bar = NSToolbar(identifier: NSToolbar.Identifier("Toolbar"))
        bar.delegate = self
        return bar
    }()
    #endif

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        let navController = window?.rootViewController as! UINavigationController
        mainCoordinator = ImageBrowseCoordinator(navigationController: navController)
        mainCoordinator.start()

        #if targetEnvironment(macCatalyst)
        if let scene = scene as? UIWindowScene, let titleBar = scene.titlebar {
            titleBar.titleVisibility = .hidden
            setToolbarVisible(true)
        }
        #endif
    }

    #if targetEnvironment(macCatalyst)
    func setToolbarVisible(_ visible: Bool) {
        if let scene = window?.windowScene, let titleBar = scene.titlebar {
            titleBar.toolbar = visible ? toolbar : nil
        }
    }
    #endif
}

#if targetEnvironment(macCatalyst)
extension SceneDelegate: NSToolbarDelegate {

    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {

        mainCoordinator.toolbarForIdentifier(id: itemIdentifier)
    }

    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {

        toolbar.items.map { $0.itemIdentifier }
    }

    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {

        []
    }

    func toolbarSelectableItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {

        []
    }

    func toolbarWillAddItem(_ notification: Notification) {


    }

    func toolbarDidRemoveItem(_ notification: Notification) {


    }

}
#endif

