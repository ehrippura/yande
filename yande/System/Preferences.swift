//
//  Preference.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/30.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation

@propertyWrapper
struct UserPreference<T> {

    private let key: String

    private let defaultValue: T

    var wrappedValue: T {
        get {
            UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }

    init(wrappedValue: T, key: String) {
        self.defaultValue = wrappedValue
        self.key = key
    }
}

enum Preferences {

    /// default base url
    @UserPreference(key: "YDBaseURL")
    static var baseUrl: String = ""

    /// post number limitation
    @UserPreference(key: "YDRequestLimit")
    static var requestLimit: Int = 50

    /// download target
    @UserPreference(key: "YDDownloadFolder")
    static var downloadFolder: String = ""
}

