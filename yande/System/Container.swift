//
//  Container.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation

enum Container {
    static let documentFolder: String = {
        #if targetEnvironment(macCatalyst)
        return FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask).first!.path
        #else
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.path
        #endif
    }()
}
