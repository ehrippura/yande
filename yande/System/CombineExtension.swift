//
//  CombineExtension.swift
//  yande
//
//  Created by Tzu-Yi Lin on 2021/10/17.
//  Copyright © 2021 EternalWind. All rights reserved.
//

import Combine

extension Publisher where Failure == Never {

    func assign<Root: AnyObject>(
        to keyPath: ReferenceWritableKeyPath<Root, Output>,
        onWeak object: Root
    ) -> AnyCancellable {
        sink { [weak object] value in
            object?[keyPath: keyPath] = value
        }
    }
}
