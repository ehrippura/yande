//
//  MainViewController.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/29.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Danbooru
import Combine

class MainViewController: GridImageViewController, StoryboardGenerate {

    weak var coordinate: ImageBrowseCoordinator?

    var postProvider: PostProvider? {

        didSet {
            providerSubscription = postProvider?.publisher()
                .replaceError(with: (post: [], page: 0))
                .receive(on: DispatchQueue.main)
                .sink { [weak self] posts, page in
                    self?.appendNewPosts(posts: posts, removeOld: page == 1)
                }
        }
    }

    var enableSearch = true

    private var providerSubscription: AnyCancellable?

    private var previousLeftButton: UIBarButtonItem?

    private var previousRightButton: UIBarButtonItem?

    @IBOutlet var selectAllButton: UIBarButtonItem!
    
    override func viewDidLoad() {

        super.viewDidLoad()

        toolbarItems = [selectAllButton]

        // add long press gesutre to enter edit mode
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        gesture.delaysTouchesBegan = true
        gesture.delegate = self
        gesture.minimumPressDuration = 0.3
        collectionView.addGestureRecognizer(gesture)

        if enableSearch {
            // add setting button
            let settingItem = UIBarButtonItem(image: UIImage(systemName: "gearshape"), style: .plain, target: self, action: #selector(showSetting))
            navigationItem.leftBarButtonItem = settingItem
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if collectionViewDataSource.snapshot().numberOfItems == 0 {
            getNextPage()
        }
        if isEditing {
            setEditing(false, animated: animated)
        }
    }

    override func setEditing(_ editing: Bool, animated: Bool) {

        collectionView.allowsMultipleSelection = editing
        collectionView.allowsMultipleSelectionDuringEditing = editing

        if editing {
            previousLeftButton = navigationItem.leftBarButtonItem
            previousRightButton = navigationItem.rightBarButtonItem
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(completeSelection(_:)))
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.down"), style: .plain, target: self, action: #selector(downloadSelected(_:)))
            navigationController?.setToolbarHidden(false, animated: animated)
        } else {
            navigationItem.leftBarButtonItem = previousLeftButton
            navigationItem.rightBarButtonItem = previousRightButton
            previousLeftButton = nil
            previousRightButton = nil
            navigationController?.setToolbarHidden(true, animated: animated)
        }
        updateTitle()

        #if targetEnvironment(macCatalyst)
        coordinate?.updateToolbarItems()
        #endif

        super.setEditing(editing, animated: animated)
    }

    @IBAction func downloadSelected(_ sender: Any?) {
        collectionView.indexPathsForSelectedItems?.forEach {
            if let post = collectionViewDataSource.itemIdentifier(for: $0) {
                imageDownloader.enqueue(post: post)
            }
        }
        completeSelection(sender)
    }

    @objc func completeSelection(_ sender: Any?) {
        collectionView.indexPathsForSelectedItems?.forEach {
            collectionView.deselectItem(at: $0, animated: true)
        }
        setEditing(false, animated: true)
    }

    @IBAction func selectAllItems(_ sender: Any?) {
        (0 ..< collectionViewDataSource.snapshot().numberOfItems)
            .forEach {
                let idx = IndexPath(item: $0, section: 0)
                collectionView.selectItem(at: idx, animated: true, scrollPosition: [])
            }
        updateTitle()
    }

    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        guard collectionViewDataSource.snapshot().numberOfItems != 0, sender.state == .began, !isEditing else {
            return
        }
        setEditing(true, animated: true)
        let point = sender.location(in: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: point) {
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        }
        updateTitle()
    }

    @IBAction func showSetting(_ sender: Any?) {
        coordinate?.showSetting()
    }

    private func getNextPage() {
        guard let dataSource = postProvider else {
            return
        }
        dataSource.nextPage()
    }

    private func updateTitle() {
        let count = collectionView.indexPathsForSelectedItems?.count ?? 0
        if count == 0 {
            navigationItem.title = title
        } else {
            navigationItem.title = "\(count) selected"
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            updateTitle()
        } else {
            let posts = collectionViewDataSource.snapshot().itemIdentifiers(inSection: .images)
            coordinate?.showDetail(posts: posts, at: indexPath.row)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isEditing {
            updateTitle()
        }
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        let count = collectionViewDataSource.snapshot().numberOfItems(inSection: .images)
        if indexPath.row > count - numberOfColumn * 2 && !isEditing {
            getNextPage()
        }
    }
}

extension MainViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
