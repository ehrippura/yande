//
//  GridImageViewController.swift
//  yande
//
//  Created by Wayne Lin on 2016/7/3.
//  Copyright © 2016 Eternal Wind. All rights reserved.
//

import UIKit
import Combine
import AVFoundation
import Danbooru

class GridImageViewController: UICollectionViewController {

    #if targetEnvironment(macCatalyst)
    let imageWidth: CGFloat = 200
    #else
    let imageWidth: CGFloat = 150
    #endif

    enum Section {
        case images
    }

    weak var imageDownloader: ImageDownloader!

    var collectionViewDataSource: UICollectionViewDiffableDataSource<Section, Post>!

    /// grid column number
    var numberOfColumn = 6 {
        didSet {
            if numberOfColumn != oldValue, let layout = collectionView?.collectionViewLayout as? PictureFlowLayout {
                layout.numberOfColumns = numberOfColumn
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let layout = collectionView?.collectionViewLayout as? PictureFlowLayout {
            layout.delegate = self
        }

        clearsSelectionOnViewWillAppear = true
        collectionView.register(UINib(nibName: "ThumbnailCell", bundle: nil), forCellWithReuseIdentifier: "ThumbnailCell")

        prepareDataSource()
        updateInterface(for: view.bounds.size)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if targetEnvironment(macCatalyst)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        #endif
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateInterface(for: size)
        collectionView.collectionViewLayout.invalidateLayout()
    }

    final func resetPosts() {
        var snapshot = collectionViewDataSource.snapshot()
        snapshot.deleteAllItems()
        snapshot.appendSections([.images])
        snapshot.appendItems([], toSection: .images)
        collectionViewDataSource.apply(snapshot, animatingDifferences: true)
    }

    final func appendNewPosts(posts: [Post], removeOld: Bool = false) {
        var snapshot = collectionViewDataSource.snapshot()

        if removeOld {
            snapshot.deleteItems(snapshot.itemIdentifiers)
        }

        let newItems = posts.filter { !snapshot.itemIdentifiers.contains($0) }
        snapshot.appendItems(newItems)
        collectionViewDataSource.apply(snapshot, animatingDifferences: true)
    }

    private func prepareDataSource() {

        let register = UICollectionView.CellRegistration<ThumbnailCell, Post>(cellNib: UINib(nibName: "ThumbnailCell", bundle: nil)) { [weak self] (cell, indexPath, post) in

            guard let strong = self else { return }

            cell.textLabel.text = "\(size: post.image.imageSize)"
            cell.imageDownloader = strong.imageDownloader
            cell.imageURL = post.thumbnail.url
        }

        collectionViewDataSource = .init(collectionView: collectionView) { (view, indexPath, post) in
            return view.dequeueConfiguredReusableCell(using: register, for: indexPath, item: post)
        }

        var defaultSnapshot = NSDiffableDataSourceSnapshot<Section, Post>()
        defaultSnapshot.appendSections([.images])
        defaultSnapshot.appendItems([])
        collectionViewDataSource.apply(defaultSnapshot, animatingDifferences: false)
    }

    private func updateInterface(for size: CGSize) {

        numberOfColumn = Int(floor(size.width / imageWidth))
    }
}

extension GridImageViewController: PictureFlowLayoutDelegate {

    func collectionView(_ collectionView: UICollectionView, heightForPictureAt indexPath: IndexPath, width: CGFloat) -> CGFloat {

        let picture = collectionViewDataSource.itemIdentifier(for: indexPath)!
        let boundingRect = CGRect(x: 0.0, y: 0.0, width: width, height: collectionView.bounds.height)
        let rect = AVMakeRect(aspectRatio: picture.thumbnail.imageSize, insideRect: boundingRect)

        return rect.height + 27.0
    }
}

