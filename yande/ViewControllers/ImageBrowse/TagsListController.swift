//
//  TagsListController.swift
//  yande
//
//  Created by Wayne Lin on 2020/7/3.
//  Copyright © 2020 EternalWind. All rights reserved.
//

import UIKit
import Combine

protocol TagsListControllerDelegate: AnyObject {
    func tagsList(_ controller: TagsListController, didSelectTag tag: String)
}

class TagsListController: UIViewController {

    enum Section {
        case main
    }

    weak var delegate: TagsListControllerDelegate?

    private var _listCollectionView: UICollectionView!

    private var _listDataSource: UICollectionViewDiffableDataSource<Section, String>!

    @Published var items: [String] = []

    private var itemUpdater: AnyCancellable?

    override func viewDidLoad() {
        super.viewDidLoad()
        createCollectionView()

        let done = UIBarButtonItem(systemItem: .done, primaryAction: UIAction(title: "", handler: { (action) in
            self.dismiss(nil)
        }), menu: nil)
        
        navigationItem.leftBarButtonItem = done
        navigationItem.title = "Tags"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureDataSource()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        _listCollectionView.collectionViewLayout.invalidateLayout()
    }

    @objc func dismiss(_ sender: Any?) {
        dismiss(animated: true, completion: nil)
    }

    private func createCollectionView() {
        _listCollectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout()!)
        _listCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        _listCollectionView.backgroundColor = .secondarySystemBackground
        _listCollectionView.delegate = self
        view.addSubview(_listCollectionView)
    }

    private func createLayout() -> UICollectionViewLayout? {

        return UICollectionViewCompositionalLayout { (sectionIndex, environment) -> NSCollectionLayoutSection? in

            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                  heightDimension: .estimated(55))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)

            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                   heightDimension: .estimated(55))

            let columnCount = environment.container.effectiveContentSize.width > 500 ? 4 : 2
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: columnCount)
            group.interItemSpacing = .fixed(8)

            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = .init(top: 10, leading: 10, bottom: 10, trailing: 10)
            section.interGroupSpacing = 10

            return section
        }
    }

    private func configureDataSource() {

        let cellRegister = UICollectionView.CellRegistration<LabelItemCell, String> { (cell, indexPath, item) in
            cell.label.text = item
        }

        _listDataSource = .init(collectionView: _listCollectionView) { (view, indexPath, item) -> UICollectionViewCell? in
            return view.dequeueConfiguredReusableCell(using: cellRegister, for: indexPath, item: item)
        }

        updateItems()
    }

    private func updateItems() {

        guard _listCollectionView != nil else {
            return
        }

        var snapshot = NSDiffableDataSourceSnapshot<Section, String>()
        snapshot.appendSections([.main])
        snapshot.appendItems(items)
        _listDataSource.apply(snapshot, animatingDifferences: true)
    }
}

extension TagsListController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let text = items[indexPath.item]
        dismiss(animated: true) {
            self.delegate?.tagsList(self, didSelectTag: text)
        }
    }
}
