//
//  TagSearchViewController.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Combine
import Danbooru

class SearchViewController: UICollectionViewController, StoryboardGenerate {
    
    private enum Scope: Int {
        case tag
        case pool
    }

    weak var coordinator: ImageBrowseCoordinator?

    private var subscriptions: Set<AnyCancellable> = []

    private var searchThrough = PassthroughSubject<String, Never>()

    private var currentScope = Scope.tag {
        didSet {
            navigationItem.rightBarButtonItem = (currentScope == .tag) ? searchTagButton : nil
            collectionView.reloadData()
        }
    }

    // tag candidates
    var tags: [Tag] = [] {
        didSet {
            if currentScope == .tag {
                collectionView.reloadData()
            }
        }
    }

    // pool candidates
    var pools: [Pool] = [] {
        didSet {
            if currentScope == .pool {
                collectionView.reloadData()
            }
        }
    }

    // save selected tags
    var searchTags: [Tag] = []

    private let numberOfColumn = 2

    private lazy var searchController: UISearchController = {
        let controller =  UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.delegate = self
        controller.obscuresBackgroundDuringPresentation = false
        controller.hidesNavigationBarDuringPresentation = false
        controller.searchBar.autocapitalizationType = .none
        controller.searchBar.delegate = self
        controller.searchBar.scopeButtonTitles = ["Tag", "Pool"]
        controller.searchBar.searchTextField.tokenBackgroundColor = UIColor.systemRed
        #if targetEnvironment(macCatalyst)
        controller.searchBar.showsScopeBar = true
        #endif
        return controller
    }()

    @IBOutlet var searchTagButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.leftBarButtonItem = .init(systemItem: .close, primaryAction: .init(handler: { [unowned self] _ in
            dismiss(animated: true, completion: nil)
        }), menu: nil)
        definesPresentationContext = true
        prepareNetworkCombine()

        #if targetEnvironment(macCatalyst)
        navigationItem.largeTitleDisplayMode = .never
        #endif
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for (index, tag) in searchTags.enumerated() {
            let token = UISearchToken(icon: UIImage(systemName: "tag"), text: tag.name)
            token.representedObject = tag
            searchController.searchBar.searchTextField.insertToken(token, at: index)
        }
        
        #if targetEnvironment(macCatalyst)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.title = ""
        navigationItem.setHidesBackButton(true, animated: false)
        #endif
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.becomeFirstResponder()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        collectionView.collectionViewLayout.invalidateLayout()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    @IBAction func beginSearchGallary() {

        var searchKeyword = searchTags.map { $0.name }
        if let searchText = searchController.searchBar.text, !searchText.isEmpty, !searchTags.map({$0.name}).contains(searchText) {
            searchKeyword.append(searchText)
        }
        coordinator?.updatePostProvider(tags: searchTags)
    }

    func clearSearch() {
        searchTags.removeAll(keepingCapacity: true)
        tags.removeAll(keepingCapacity: true)
        collectionView.reloadData()
        coordinator?.updatePostProvider(tags: [])
    }

    private func prepareNetworkCombine() {

        guard let client = coordinator?.networkClient else {
            return
        }

        let publisher = searchThrough
            .debounce(for: 0.35, scheduler: DispatchQueue.main)

        publisher
            .flatMap { query in
                client.tags(query: query)
            }
            .replaceError(with: [])
            .receive(on: OperationQueue.main)
            .assign(to: \.tags, onWeak: self)
            .store(in: &subscriptions)

        publisher
            .flatMap { query in
                client.poolInfos(query: query)
            }
            .replaceError(with: [])
            .receive(on: OperationQueue.main)
            .assign(to: \.pools, onWeak: self)
            .store(in: &subscriptions)
    }

    // MARK: - UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch currentScope {
        case .tag:
            return tags.count
        case .pool:
            return pools.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LabelItemCell.reuseIdentifier, for: indexPath) as! LabelItemCell

        switch currentScope {
        case .tag:
            let tag = tags[indexPath.item]
            cell.label.text = tag.name
            cell.label.textColor = tag.color
            if searchTags.contains(tag) {
                cell.contentView.backgroundColor = .systemGray5
            }

        case .pool:
            let pool = pools[indexPath.item]
            cell.label.text = pool.name
            cell.label.textColor = UIColor.label
        }

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch currentScope {
        case .tag:
            let tag = tags[indexPath.item]
            if !searchTags.contains(tag) {
                let token = UISearchToken(icon: UIImage(systemName: "tag"), text: tag.name)
                token.representedObject = tag
                searchController.searchBar.searchTextField.insertToken(token, at: searchTags.count)
                searchController.searchBar.text = nil
                collectionView.reloadItems(at: [indexPath])
            } else {
                let idx = searchController.searchBar.searchTextField.tokens.firstIndex {
                    ($0.representedObject as! Tag) == tag
                }
                if let idx = idx {
                    searchController.searchBar.searchTextField.removeToken(at: idx)
                }
            }
            beginSearchGallary()

        case .pool:
            let pool = pools[indexPath.item]
            coordinator?.displayPost(pool: pool)
        }
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else {
            fatalError("Flow Layout")
        }

        let width = (collectionView.frame.width - layout.minimumInteritemSpacing * CGFloat(numberOfColumn + 1)) / CGFloat(numberOfColumn)

        let text: NSString
        switch currentScope {
        case .tag:
            text = tags[indexPath.row].name as NSString
        case .pool:
            text = pools[indexPath.row].name as NSString
        }

        let bounds = text.boundingRect(with: CGSize(width: width - 16, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [.font : UIFont.preferredFont(forTextStyle: .body)], context: nil)

        return CGSize(width: width, height: bounds.height + 16)
    }
}

extension SearchViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {

        if searchTags.count != searchController.searchBar.searchTextField.tokens.count {
            searchTags = searchController.searchBar.searchTextField.tokens.map { $0.representedObject as! Tag }
            collectionView.reloadData()
            beginSearchGallary()
        }

        guard let searchString = searchController.searchBar.text, !searchString.isEmpty else {
            return
        }

        if let keyword = searchController.searchBar.text, !keyword.isEmpty {
            searchThrough.send(keyword)
        }
    }
}

extension SearchViewController: UISearchBarDelegate, UISearchControllerDelegate {

    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if currentScope == .tag {
            beginSearchGallary()
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        clearSearch()
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        currentScope = Scope(rawValue: selectedScope)!
        if currentScope == .pool {
            searchController.searchBar.searchTextField.tokens = []
        }
    }
}
