//
//  PostDetailViewController.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/5.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Combine
import Danbooru

class PostDetailViewController: UIViewController, StoryboardGenerate {

    @IBOutlet weak var imageView: ImageScrollView!

    @IBOutlet weak var progressBar: UIProgressView!

    private let downloader = ImageDownloader()

    private var cancellable: AnyCancellable?

    var post: Post? {
        didSet {
            guard let post = post else { return }
            title = "\(post.id)"
            if isViewLoaded {
                downloadImage()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        downloadImage()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let rate = imageView.zoomScale / imageView.minimumZoomScale
        imageView.updateZoomScaleSettings()
        imageView.zoomScale = rate * imageView.minimumZoomScale
    }

    func downloadImage() {
        guard let post = post else { return }

        cancellable = downloader.image(url: post.sample.url) { [weak self] (progress) in
            let prog = Float(progress.completedUnitCount) / Float(progress.totalUnitCount)
            self?.progressBar.setProgress(prog, animated: true)
        }
        .receive(on: DispatchQueue.main)
        .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] image in
            self?.view.layoutIfNeeded()
            self?.imageView.image = image
            self?.progressBar.isHidden = true
        })
    }
}
