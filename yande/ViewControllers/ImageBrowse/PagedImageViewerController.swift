//
//  PagedImageViewerController.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/5.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Danbooru

class PagedImageViewerController: UIViewController, StoryboardGenerate {

    var posts: [Post] = []

    var currentIndex: Int = 0

    private var transitionTarget: PostDetailViewController?

    weak var imageDownloader: ImageDownloader?

    weak var coordinator: ImageBrowseCoordinator?

    private var imagePageController: UIPageViewController? {
        didSet {
            imagePageController?.delegate = self
            imagePageController?.dataSource = self
        }
    }

    private var navigationBarVisibile = true {
        didSet {
            hideNavigationBar(!navigationBarVisibile)
            setNeedsUpdateOfHomeIndicatorAutoHidden()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let postController = PostDetailViewController.instaniate()
        postController.post = posts[currentIndex]
        imagePageController?.setViewControllers([postController], direction: .forward, animated: false, completion: nil)
        title = postController.title

        let gesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }

    override var prefersStatusBarHidden: Bool {
        return !navigationBarVisibile
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }

    override var prefersHomeIndicatorAutoHidden: Bool {
        return !navigationBarVisibile
    }

    private func hideNavigationBar(_ hide: Bool) {

        #if !targetEnvironment(macCatalyst)
        navigationController?.setNavigationBarHidden(hide, animated: true)
        setNeedsStatusBarAppearanceUpdate()
        #else
        if let delegate = view.window?.windowScene?.delegate as? SceneDelegate {
            delegate.setToolbarVisible(!hide)
        }
        #endif

        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = hide ? .black : UIColor.systemBackground
        }
    }

    @objc func tap() {
        navigationBarVisibile = !navigationBarVisibile
    }

    @IBAction func tagListPage(_ sender: Any?) {
        let post = posts[currentIndex]
        let controller = TagsListController()
        controller.delegate = self
        controller.items = post.tags
        present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
    }

    @IBAction func downloadFullsizeImage(_ sender: Any?) {
        let post = posts[currentIndex]
        imageDownloader?.enqueue(post: post)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? UIPageViewController {
            imagePageController = controller
        } else {
            fatalError("embabbed page controller missing")
        }
    }
}

extension PagedImageViewerController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let detail = viewController as? PostDetailViewController,
            let idx = posts.firstIndex(of: detail.post!), idx != 0 else {
            return nil
        }

        let newDetail = PostDetailViewController.instaniate()
        newDetail.post = posts[idx - 1]

        return newDetail
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let detail = viewController as? PostDetailViewController,
            let idx = posts.lastIndex(of: detail.post!), idx + 1 < posts.count else {
            return nil
        }

        let newDetail = PostDetailViewController.instaniate()
        newDetail.post = posts[idx + 1]

        return newDetail
    }

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {

        transitionTarget = pendingViewControllers.first as? PostDetailViewController
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if completed {
            if let target = transitionTarget {
                currentIndex = posts.firstIndex(of: target.post!)!
                title = target.title
            }
        }

        transitionTarget = nil
    }
}

extension PagedImageViewerController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        return otherGestureRecognizer is UITapGestureRecognizer
    }
}

extension PagedImageViewerController: TagsListControllerDelegate {
    func tagsList(_ controller: TagsListController, didSelectTag tag: String) {
        coordinator?.displayPost(keyword: [tag])
    }
}
