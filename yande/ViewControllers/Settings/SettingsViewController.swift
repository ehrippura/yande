//
//  SettingsViewController.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/14.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController, StoryboardGenerate {

    static var storyboardName: String {
        return "Settings"
    }

    weak var coordinator: SettingsCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = true
    }

    @IBAction func doneButtonAction(_ sender: Any?) {
        coordinator?.completeSetting()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 0 {
            coordinator?.showDownloadQueue()
        }
    }
}
