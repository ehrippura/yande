//
//  DownloadQueueViewController.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/14.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

class DownloadQueueViewController: UITableViewController, StoryboardGenerate  {

    static let storyboardName: String = "Settings"
    
    var paddingQueue: [ImageDownloader.DownloadPair] = []

    weak var coordinator: SettingsCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()

        // add queue listener
        paddingQueue.enumerated().forEach { (arg) in
            let (offset, element) = arg
            element.request.downloadProgress { [weak self] (progress) in
                guard let cell = self?.tableView.cellForRow(at: IndexPath(row: offset, section: 0)) as? DownloadQueueCell else {
                    return
                }
                cell.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paddingQueue.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "QueueCell", for: indexPath) as! DownloadQueueCell

        let pair = paddingQueue[indexPath.row]

        cell.nameLabel?.text = "yande \(pair.post.id)"
        cell.urlLabel?.text = pair.post.image.url.absoluteString
        cell.progressBar.setProgress(Float(pair.1.downloadProgress.fractionCompleted), animated: false)

        return cell
    }
}
