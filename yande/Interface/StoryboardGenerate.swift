//
//  StoryboardGenerate.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/29.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

protocol StoryboardGenerate {
    static var storyboardName: String { get }
    static func instaniate() -> Self
}

extension StoryboardGenerate where Self: UIViewController {

    static var storyboardName: String {
        return "Main"
    }

    static func instaniate() -> Self {
        return instaniate(from: storyboardName)
    }

    private static func instaniate(from name: String) -> Self {
        let identifier = String(describing: self)
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
}
