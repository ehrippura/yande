//
//  PostDataSource.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Danbooru
import Combine
import Alamofire

protocol PostProvider {
    var title: String? { get }
    func publisher() -> AnyPublisher<(post: [Post], page: Int), AFError>
    func nextPage()
}
