//
//  ImageDownloder.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Combine
import Alamofire
import Danbooru

class ImageDownloader {

    enum ImageDownloaderError: Error {
        case decode
        case framework(AFError)
    }

    typealias DownloadPair = (post: Post, request: DownloadRequest)

    private var downloadQueue: [DownloadPair] = []

    var paddingQueue: [DownloadPair] {
        valueLock.lock(); defer { valueLock.unlock() }
        return downloadQueue
    }

    private var valueLock = NSLock()

    private let sessionManager: Session = Session(configuration: {
        let conf = URLSessionConfiguration.default
        conf.httpMaximumConnectionsPerHost = 4
        return conf
    }())

    private var keepsRunning = false {
        didSet {
            if !keepsRunning {
                threadSemaphore.signal()
            }
        }
    }

    /// pause thread
    private var threadSemaphore: DispatchSemaphore = DispatchSemaphore(value: 0)

    @discardableResult
    func enqueue(post: Post) -> DownloadRequest {

        valueLock.lock(); defer { valueLock.unlock() }

        if !keepsRunning {
            enableDownloadQueue()
        }

        let request = sessionManager.download(
            post.image.url,
            to:  { (url, response) -> (URL, DownloadRequest.Options) in
                let filePath = (Container.documentFolder as NSString)
                    .appendingPathComponent("\(filename: post)")
                return (URL(fileURLWithPath: filePath), [])
            }
        )

        request.response { [weak self] _ in
            if let idx = self?.downloadQueue.firstIndex(where: { $0.post == post }) {
                self?.downloadQueue.remove(at: idx)
            }
        }

        downloadQueue.append((post, request))
        threadSemaphore.signal()

        return request
    }

    func disableDownloadQueue() {
        valueLock.lock()
        downloadQueue.forEach { $0.request.cancel() }
        downloadQueue.removeAll()
        valueLock.unlock()
        keepsRunning = false
    }

    private func enableDownloadQueue() {

        Thread.detachNewThread { [weak self] in

            guard let strongSelf = self else {
                return
            }

            Thread.current.name = "download.thread"

            self?.valueLock.lock()
            strongSelf.keepsRunning = true
            self?.valueLock.unlock()

            while(strongSelf.keepsRunning) {

                strongSelf.threadSemaphore.wait()

                self?.valueLock.lock()
                var request: DownloadRequest?
                if strongSelf.downloadQueue.count > 0 {
                    request = strongSelf.downloadQueue.first!.request
                }
                self?.valueLock.unlock()

                if let request = request {
                    request.resume()
                }
            }
        }
    }

    /// get image
    /// result contains image object and boolean value to represent that image is cames form caches or not.
    @discardableResult
    func image(url: URL, progressHandle handle:((Progress) -> Void)? = nil) -> AnyPublisher<UIImage, ImageDownloaderError> {

        return Future { [weak self] promise in

            let request = self?.sessionManager.request(url).responseData { (result) in

                switch result.result {
                case .success(let data):
                    if let image = UIImage(data: data) {
                        promise(.success(image))
                    } else {
                        promise(.failure(ImageDownloaderError.decode))
                    }

                case .failure(let error):
                    promise(.failure(.framework(error)))
                    NSLog("cannot download image: \(url), error: \(error.localizedDescription)")
                }
            }

            if let handle = handle {
                request?.downloadProgress(closure: handle)
            }
        }
        .eraseToAnyPublisher()
    }
}
