//
//  KeywordDataSource.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Danbooru
import Alamofire
import Combine

class KeywordDataSource: PostProvider {

    @Published var keyword: [String] = []

    @Published var completed: Bool = false

    @Published private(set) var title: String?

    private var client: DanbooruClient

    private var isLoading: Bool = false

    private let postSubject: CurrentValueSubject<Int, Never> = .init(0)

    private var subscriptions: Set<AnyCancellable> = []

    init(client: DanbooruClient, keyword: [String] = []) {
        self.client = client
        self.keyword = keyword
        prepareCombine()
    }

    private func prepareCombine() {

        $keyword
            .map {
                $0.joined(separator: " ")
            }
            .map {
                Optional.some($0)
            }
            .assign(to: &$title)

        $keyword
            .dropFirst()
            .sink { [unowned self] keyword in
                completed = false
                isLoading = false
                postSubject.send(1)
            }
            .store(in: &subscriptions)
    }

    private func postPublisher() -> AnyPublisher<[Post], AFError> {

        let limit = Preferences.requestLimit

        return postSubject
            .filter { page in
                page != 0
            }
            .filter { [unowned self] _ in !isLoading && !completed }
            .handleEvents(receiveOutput: { _ in
                self.isLoading = true
            })
            .combineLatest($keyword)
            .map { [unowned self] (page, keyword) -> AnyPublisher<[Post], AFError> in
                client.posts(page: page, limit: limit, keyword: keyword)
            }
            .switchToLatest()
            .handleEvents(receiveOutput: { [unowned self] posts in
                if posts.isEmpty {
                    completed = true
                }
                isLoading = false
            })
            .eraseToAnyPublisher()
    }

    func publisher() -> AnyPublisher<(post: [Post], page: Int), AFError> {

        let pagePublisher = postSubject
            .filter { $0 != 0 }
            .setFailureType(to: AFError.self)

        return Publishers.Zip(postPublisher(), pagePublisher)
            .map {
                (post: $0, page: $1)
            }
            .eraseToAnyPublisher()
    }

    func nextPage() {
        guard !isLoading else { return }
        let currnetPage = postSubject.value
        postSubject.send(currnetPage + 1)
    }
}

extension KeywordDataSource: CustomDebugStringConvertible {
    var debugDescription: String {
        "Keyboard post provider with: \(keyword)"
    }
}
