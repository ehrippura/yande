//
//  PoolDataSource.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Danbooru
import Combine
import Alamofire

class PoolDataSource: PostProvider {

    var title: String? {
        poolInfo.name
    }

    let poolInfo: Pool

    let client: DanbooruClient

    private let postSubject = PassthroughSubject<Int, Never>()

    init(client: DanbooruClient, poolInfo: Pool) {
        self.poolInfo = poolInfo
        self.client = client
    }

    func nextPage() {
        postSubject.send(poolInfo.id)
        postSubject.send(completion: .finished)
    }

    func publisher() -> AnyPublisher<(post: [Post], page: Int), AFError> {
        postSubject
            .flatMap { id -> AnyPublisher<[Post], AFError> in
                self.client.posts(poolId: id)
            }
            .map {
                (post: $0, page: 0)
            }
            .eraseToAnyPublisher()
    }
}
