//
//  TagColorfy.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/8.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Danbooru

extension Tag {
    var color: UIColor {
        switch type {
        case .artist:
            return UIColor.systemYellow
        case .copyright:
            return UIColor.magenta
        case .character:
            return UIColor.systemGreen
        case .circle:
            return UIColor.systemBlue
        default:
            return UIColor.label
        }
    }
}
