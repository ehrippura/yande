//
//  Coordinate.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/29.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {

    var children: [Coordinator] { set get }

    var parent: Coordinator? { get set }

    func start()
}

extension Coordinator {
    func removeFromParent() {
        guard let parent = parent else {
            return
        }
        if let idx = parent.children.firstIndex(where: { $0 === self}) {
            parent.children.remove(at: idx)
        }
    }
}

protocol NavigationCoordinator: Coordinator, UINavigationControllerDelegate {

    var navigationController: UINavigationController { get }

    init(navigationController: UINavigationController)
}
