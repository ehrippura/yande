//
//  ImageBrowseCoordinate.swift
//  yande
//
//  Created by Wayne Lin on 2019/3/29.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Danbooru

class ImageBrowseCoordinator: NSObject, NavigationCoordinator {

    var children: [Coordinator] = []

    weak var parent: Coordinator?

    let navigationController: UINavigationController

    private var presentedViewController: UIViewController?

    private(set) var networkClient: DanbooruClient

    private(set) var imageDownloader: ImageDownloader
    
    /// save tags using for main page filter
    private var searchingTags: [Tag] = []
    
    private var mainController: MainViewController {
        navigationController.viewControllers.first as! MainViewController
    }

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        let defaultClient = Preferences.baseUrl
        self.networkClient = try! DanbooruClient(baseUrl: defaultClient)
        self.imageDownloader = ImageDownloader()
        super.init()
        navigationController.delegate = self
        navigationController.navigationBar.prefersLargeTitles = true
    }

    func start() {
        mainController.coordinate = self
        mainController.postProvider = KeywordDataSource(client: networkClient, keyword: [])
        let searchItem = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(search(_:)))
        mainController.navigationItem.rightBarButtonItem = searchItem
        mainController.imageDownloader = imageDownloader
    }
    
    func updatePostProvider(tags: [Tag]) {
        let provider = KeywordDataSource(client: networkClient, keyword: tags.map({ $0.name }))
        mainController.postProvider = provider
        searchingTags = tags
        provider.nextPage()
    }

    func displayPost(keyword: [String]) {
        let mainController = MainViewController.instaniate()
        mainController.coordinate = self
        mainController.postProvider = KeywordDataSource(client: networkClient, keyword: keyword)
        mainController.imageDownloader = imageDownloader
        mainController.enableSearch = false
        navigationController.pushViewController(mainController, animated: true)
    }

    func displayPost(pool: Pool) {
        let mainController = MainViewController.instaniate()
        mainController.coordinate = self
        mainController.postProvider = PoolDataSource(client: networkClient, poolInfo: pool)
        mainController.imageDownloader = imageDownloader
        mainController.enableSearch = false
        navigationController.pushViewController(mainController, animated: true)
    }

    @objc func search(_ sender: Any?) {
        let searchController = SearchViewController.instaniate()
        searchController.coordinator = self
        searchController.searchTags = searchingTags
        let targetToPresent = UINavigationController(rootViewController: searchController)
        if let sheet = targetToPresent.sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.prefersGrabberVisible = true
        }
        navigationController.present(targetToPresent, animated: true, completion: nil)
    }

    func showSetting() {
        let navController = UINavigationController()
        let settings = SettingsCoordinator(navigationController: navController)
        settings.parent = self
        settings.imageDownloader = imageDownloader
        children.append(settings)
        settings.start()
        navigationController.present(navController, animated: true, completion: nil)
    }

    func showDetail(posts: [Post], at index: Int) {
        let pageController = PagedImageViewerController.instaniate()
        pageController.posts = posts
        pageController.currentIndex = index
        pageController.imageDownloader = imageDownloader
        pageController.coordinator = self
        navigationController.pushViewController(pageController, animated: true)
    }

    #if targetEnvironment(macCatalyst)
    func updateToolbarItems() {

        guard let scene = navigationController.view.window?.windowScene,
              let toolbar = scene.titlebar?.toolbar else {
            return
        }

        for _ in (0..<toolbar.items.count) {
            toolbar.removeItem(at: 0)
        }

        var insertCount = 0

        if let leftItems = presentedViewController?.navigationItem.leftBarButtonItems {
            for i in (0..<leftItems.count) {
                toolbar.insertItem(withItemIdentifier: NSToolbarItem.Identifier("item_left_\(i)"), at: insertCount)
                insertCount += 1
            }
        } else if presentedViewController !== navigationController.viewControllers.first {
            toolbar.insertItem(withItemIdentifier: NSToolbarItem.Identifier("back_item"), at: 0)
            insertCount += 1
        }

        toolbar.insertItem(withItemIdentifier: .flexibleSpace, at: insertCount)
        insertCount += 1

        if let rightItems = presentedViewController?.navigationItem.rightBarButtonItems {
            for i in (0..<rightItems.count) {
                toolbar.insertItem(withItemIdentifier: NSToolbarItem.Identifier("item_right_\(i)"), at: insertCount)
                insertCount += 1
            }
        }

        if let title = presentedViewController?.navigationItem.title, !title.isEmpty {
            scene.title = title
        } else {
            scene.title = ""
        }
    }

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        presentedViewController = viewController
        updateToolbarItems()
    }

    func toolbarForIdentifier(id: NSToolbarItem.Identifier) -> NSToolbarItem {

        switch id.rawValue {
        case "back_item":
            let item = UIBarButtonItem(image: UIImage(systemName: "chevron.left"),
                                       style: .plain, target: self, action: #selector(popNavigationItem))
            return NSToolbarItem(itemIdentifier: id, barButtonItem: item)

        case let x where x.hasPrefix("item_left_"):
            let sub = x[x.index(x.lastIndex(of: "_")!, offsetBy: 1)...]
            let item = presentedViewController?.navigationItem.leftBarButtonItems?[Int(String(sub))!]
            let toolbarItem = NSToolbarItem(itemIdentifier: id, barButtonItem: item!)
            return toolbarItem

        case let x where x.hasPrefix("item_right_"):
            let sub = x[x.index(x.lastIndex(of: "_")!, offsetBy: 1)...]
            let item = presentedViewController?.navigationItem.rightBarButtonItems?[Int(String(sub))!]
            let toolbarItem = NSToolbarItem(itemIdentifier: id, barButtonItem: item!)
            return toolbarItem

        default:
            return NSToolbarItem(itemIdentifier: id)
        }
    }

    @objc func popNavigationItem() {
        navigationController.popViewController(animated: true)
    }

    #endif
}
