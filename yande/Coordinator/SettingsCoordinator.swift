//
//  SettingsCoordinator.swift
//  yande
//
//  Created by Wayne Lin on 2019/4/14.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit
import Combine

class SettingsViewModel: ObservableObject {
    
}

class SettingsCoordinator: NSObject, NavigationCoordinator {

    var children: [Coordinator] = []

    weak var parent: Coordinator?

    var navigationController: UINavigationController

    var imageDownloader: ImageDownloader?

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let settingController = SettingsViewController.instaniate()
        settingController.coordinator = self
        navigationController.pushViewController(settingController, animated: false)
    }

    func showDownloadQueue() {
        let controller = DownloadQueueViewController.instaniate()
        controller.coordinator = self
        controller.paddingQueue = imageDownloader?.paddingQueue ?? []
        navigationController.pushViewController(controller, animated: true)
    }

    func completeSetting() {
        navigationController.presentingViewController?.dismiss(animated: true) { [weak self] in
            self?.removeFromParent()
        }
    }
}
