//
//  Dependencies.h
//  Dependencies
//
//  Created by Wayne Lin on 2020/7/5.
//  Copyright © 2020 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AlamofireDynamic.
FOUNDATION_EXPORT double AlamofireDynamicVersionNumber;

//! Project version string for AlamofireDynamic.
FOUNDATION_EXPORT const unsigned char AlamofireDynamicVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Dependencies/PublicHeader.h>


