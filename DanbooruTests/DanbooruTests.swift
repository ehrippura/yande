//
//  DanbooruTests.swift
//  DanbooruTests
//
//  Created by Wayne Lin on 2019/3/29.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import XCTest
import Combine
@testable import Danbooru

class DanbooruTests: XCTestCase {

    var client: DanbooruClient!

    private var subscriptions: Set<AnyCancellable> = []

    override func setUp() {
        let baseUrl = "https://yande.re"
        client = try? DanbooruClient(baseUrl: baseUrl)
        XCTAssertNotNil(client, "client creation failed")
    }

    func testPostCompare() {

        let jsonObject: Data = #"""
            {
                "id": 527794,
                "tags": "dress hentai_ouji_to_warawanai_neko kantoku monochrome pantsu see_through sketch skirt_lift summer_dress tagme",
                "created_at": 1553455711,
                "updated_at": 1553600232,
                "creator_id": 220654,
                "approver_id": null,
                "author": "kiyoe",
                "change": 2768064,
                "source": "変態王子と笑わない猫。13",
                "score": 10,
                "md5": "76d80e967fb42ed644131502b6ad4936",
                "file_size": 601516,
                "file_ext": "jpg",
                "file_url": "https://files.yande.re/image/76d80e967fb42ed644131502b6ad4936/yande.re%20527794%20dress%20hentai_ouji_to_warawanai_neko%20kantoku%20monochrome%20pantsu%20see_through%20sketch%20skirt_lift%20summer_dress%20tagme.jpg",
                "is_shown_in_index": true,
                "preview_url": "https://assets.yande.re/data/preview/76/d8/76d80e967fb42ed644131502b6ad4936.jpg",
                "preview_width": 106,
                "preview_height": 150,
                "actual_preview_width": 211,
                "actual_preview_height": 300,
                "sample_url": "https://files.yande.re/sample/76d80e967fb42ed644131502b6ad4936/yande.re%20527794%20sample%20dress%20hentai_ouji_to_warawanai_neko%20kantoku%20monochrome%20pantsu%20see_through%20sketch%20skirt_lift%20summer_dress%20tagme.jpg",
                "sample_width": 1056,
                "sample_height": 1500,
                "sample_file_size": 207349,
                "jpeg_url": "https://files.yande.re/image/76d80e967fb42ed644131502b6ad4936/yande.re%20527794%20dress%20hentai_ouji_to_warawanai_neko%20kantoku%20monochrome%20pantsu%20see_through%20sketch%20skirt_lift%20summer_dress%20tagme.jpg",
                "jpeg_width": 1442,
                "jpeg_height": 2048,
                "jpeg_file_size": 0,
                "rating": "s",
                "is_rating_locked": false,
                "has_children": false,
                "parent_id": null,
                "status": "active",
                "is_pending": false,
                "width": 1442,
                "height": 2048,
                "is_held": false,
                "frames_pending_string": "",
                "frames_pending": [],
                "frames_string": "",
                "frames": [],
                "is_note_locked": false,
                "last_noted_at": 0,
                "last_commented_at": 0
            }
        """#.data(using: .utf8)!

        let decoder = JSONDecoder()
        do {
            let post1 = try decoder.decode(Post.self, from: jsonObject)
            var post2 = try decoder.decode(Post.self, from: jsonObject)
            XCTAssertEqual(post1, post2, "post should be equal")

            post2.id = 100
            XCTAssertNotEqual(post1, post2, "post should not be equal")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testClientCreationFailed() {
        let url = "アイウエオ"
        XCTAssertThrowsError(try DanbooruClient(baseUrl: url))
    }

    func testGetPost() {

        let exp = expectation(description: "get post")

        client.posts(page: 1, limit: 1, keyword: nil)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                }
                exp.fulfill()
            }, receiveValue: { posts in
                XCTAssertTrue(!posts.isEmpty, "empty result")
            })
            .store(in: &subscriptions)

        waitForExpectations(timeout: 30) { (error) in
            XCTAssertNil(error, error!.localizedDescription)
        }
    }

}
