
//
//  PreferenceTest.swift
//  yandeTests
//
//  Created by Wayne Lin on 2019/3/30.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import XCTest
@testable import yande

class PreferenceTest: XCTestCase {

    override func setUp() {

    }

    override func tearDown() {

    }

    func testRegisterDefaultPreference() {
        let result = UserDefaults.standard.configureDefaultPreference(resourceName: "Default")
        XCTAssert(result, "config default preference failed")
    }

}
