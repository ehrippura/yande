//
//  API.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/1.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation
import Alamofire

enum API: RequestParameter {

    case post(page: Int, limit: Int, keywords: [String]?)

    case tagSearch(keyword: String)

    case poolSearch(keyword: String?)

    case poolPosts(poolId: Int)

    var path: String {
        switch self {
        case .post:
            return "post.json"

        case .tagSearch:
            return "tag.json"

        case .poolSearch:
            return "pool.json"

        case .poolPosts:
            return "pool/show.json"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters {
        switch self {
        case .post(let page, let limit, let keywords):
            var params: Parameters = ["page" : page, "limit" : limit]
            if let keywords = keywords {
                params["tags"] = keywords.joined(separator: " ")
            }
            return params

        case .tagSearch(let keyword):
            return ["name" : keyword, "order" : "count", "limit" : 200]

        case .poolSearch(let keyword):
            if let keyword = keyword {
                return ["query" : keyword]
            }
            return [:]

        case .poolPosts(let poolId):
            return ["id" : poolId]
        }
    }
}
