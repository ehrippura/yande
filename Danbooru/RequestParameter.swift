//
//  RequestParameter.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Alamofire

protocol RequestParameter {

    var path: String { get }

    var method: HTTPMethod { get }

    var parameters: Parameters { get }
}
