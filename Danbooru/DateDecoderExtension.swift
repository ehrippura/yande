//
//  DateDecoderExtension.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/10.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation

enum DateError: Error {
    case invalidDate
}

extension JSONDecoder.DateDecodingStrategy {
    static func extendedDateDecode() -> JSONDecoder.DateDecodingStrategy {

        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)

        return .custom { (decoder) -> Date in
            
            let container = try decoder.singleValueContainer()
            let dateStr = try container.decode(String.self)

            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            if let date = formatter.date(from: dateStr) {
                return date
            }

            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
            if let date = formatter.date(from: dateStr) {
                return date
            }

            throw DateError.invalidDate
        }
    }
}
