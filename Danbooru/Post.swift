//
//  PictureSource.swift
//  yande
//
//  Created by Wayne Lin on 2014/7/20.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import UIKit

public struct Post: Decodable, Identifiable, Hashable {

    public var id: UInt = 0

    public var tags: [String]

    public var thumbnail: ImageInfo

    public var sample: ImageInfo

    public var image: ImageInfo

    public var imageFileSize: Int64

    public var fileExtension: String?

    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(UInt.self, forKey: CodingKeys.id)

        let tags = try container.decode(String.self, forKey: CodingKeys.tags)
        self.tags = tags.split(separator: " ").map(String.init)

        var filepath = try container.decode(String.self, forKey: CodingKeys.preview_url)
        var filesize = CGSize(width: try container.decode(Double.self, forKey: CodingKeys.preview_width),
                              height: try container.decode(Double.self, forKey: CodingKeys.preview_height))
        thumbnail = ImageInfo(url: URL(string: filepath)!, imageSize: filesize)

        filepath = try container.decode(String.self, forKey: CodingKeys.sample_url)
        filesize = CGSize(width: try container.decode(Double.self, forKey: CodingKeys.sample_width),
                          height: try container.decode(Double.self, forKey: CodingKeys.sample_height))
        sample = ImageInfo(url: URL(string: filepath)!, imageSize: filesize)

        filepath = try container.decode(String.self, forKey: CodingKeys.file_url)
        filesize = CGSize(width: try container.decode(Double.self, forKey: CodingKeys.width),
                          height: try container.decode(Double.self, forKey: CodingKeys.height))
        image = ImageInfo(url: URL(string: filepath)!, imageSize: filesize)

        imageFileSize = try container.decode(Int64.self, forKey: CodingKeys.file_size)

        fileExtension = try? container.decode(String.self, forKey: CodingKeys.file_ext)
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    public static func ==(lhs: Post, rhs: Post) -> Bool {
        return lhs.id == rhs.id
    }

    enum CodingKeys: String, CodingKey {
        case id
        case tags
        case preview_url
        case preview_width
        case preview_height
        case sample_url
        case sample_width
        case sample_height
        case file_url
        case width
        case height
        case file_size
        case file_ext
    }
}
