//
//  Pool.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/10.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation

public struct Pool: Decodable, Identifiable, Hashable {

    public var id: Int

    public var name: String

    public var createdDate: Date

    public var updatedDate: Date

    public var user: Int

    public var isPublic: Bool

    public var postCount: Int

    public var description: String

    public var posts: [Post]?

    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case createdDate = "created_at"
        case updatedDate = "updated_at"
        case user = "user_id"
        case isPublic = "is_public"
        case postCount = "post_count"
        case description = "description"
        case posts
    }
}
