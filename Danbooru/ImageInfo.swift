//
//  ImageInfo.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/4.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import UIKit

public struct ImageInfo {

    public var url: URL

    public var imageSize: CGSize
}
