//
//  DanbooruClient.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/3/30.
//  Copyright © 2019 EternalWind. All rights reserved.
//

import Foundation
import Combine
import Alamofire

public final class DanbooruClient {

    public enum ClientError: Error {
        case url
        case certification
    }

    public let baseUrl: URL

    private let processingQueue = DispatchQueue(label: "DanbooruClient",
                                                qos: .background,
                                                attributes: [.concurrent])

    private let sessionManager: Session = Session(configuration: URLSessionConfiguration.default)

    /// create danbooru client with base url
    public init(baseUrl: String) throws {
        guard let url = URL(string: baseUrl) else {
            throw ClientError.url
        }
        self.baseUrl = url
    }

    /// get post list with keywords (optional)
    public func posts(page: Int, limit: Int, keyword: [String]?) -> AnyPublisher<[Post], AFError> {

        let api = API.post(page: page, limit: limit, keywords: keyword)

        return request(parameter: api)
            .publishDecodable(type: [Post].self, queue: processingQueue, decoder: JSONDecoder())
            .value()
            .eraseToAnyPublisher()
    }

    /// get posts from pool
    public func posts(poolId: Int) -> AnyPublisher<[Post], AFError> {

        let api = API.poolPosts(poolId: poolId)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .extendedDateDecode()

        return request(parameter: api)
            .publishDecodable(type: Pool.self, queue: processingQueue, decoder: decoder)
            .value()
            .map(\.posts)
            .replaceNil(with: [])
            .eraseToAnyPublisher()
    }

    /// get tag
    public func tags(query: String) -> AnyPublisher<[Tag], AFError> {
        let api = API.tagSearch(keyword: query)
        return request(parameter: api)
            .publishDecodable(type: [Tag].self, queue: processingQueue, decoder: JSONDecoder())
            .value()
            .eraseToAnyPublisher()
    }

    /// get pool list
    public func poolInfos(query: String?) -> AnyPublisher<[Pool], AFError> {

        let api = API.poolSearch(keyword: query)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .extendedDateDecode()

        return request(parameter: api)
            .publishDecodable(type: [Pool].self, queue: processingQueue, decoder: decoder)
            .value()
            .eraseToAnyPublisher()
    }

    /// send request
    private func request(parameter: RequestParameter) -> DataRequest {

        return sessionManager.request(baseUrl.appendingPathComponent(parameter.path),
                                      method: parameter.method,
                                      parameters: parameter.parameters,
                                      encoding: URLEncoding.default)
            .validate(statusCode: 200..<300)
    }
}
