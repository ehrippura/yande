//
//  Tag.swift
//  Danbooru
//
//  Created by Wayne Lin on 2019/4/7.
//  Copyright © 2019 EternalWind. All rights reserved.
//

public struct Tag: Codable, Equatable, Identifiable {

    public enum TagType: Int, Codable {
        case general = 0
        case artist = 1
        case padding_1 = 2
        case copyright = 3
        case character = 4
        case circle = 5
        case padding_3 = 6
    }

    public var id: Int

    public var name: String

    public var type: TagType

    public var count: Int

    public static func ==(lhs: Tag, rhs: Tag) -> Bool {
        return lhs.id == rhs.id
    }
}
